// import '@babel/polyfill'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Ant from 'ant-design-vue'
import './assets/css/default.less'
import dink from 'dinkjs'
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'
import './helper/core'

import config from './helper/config'
import http from './helper/http'
import helper from './helper/helper'
import * as echarts from 'echarts'
import i18n from '@/common/lang/index'
import { setupGlobDirectives } from './directives'
Vue.prototype.$bus = new Vue()
Vue.config.productionTip = false
Vue.prototype.$echarts = echarts
Vue.use(Ant)
Vue.use(dink)

Vue.use(Viewer)
Vue.prototype.$config = config
Vue.prototype.$http = http
Vue.prototype.$helper = helper
setupGlobDirectives(Vue)
new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
  data: {
    Bus: new Vue()
  }
}).$mount('#app')
