import Vue, { TrackOpTypes } from 'vue'
import helper from '@/helper/helper'
import permission from '@/helper/permission'
import store from '@/store'
// import '@/mock/index.js'
import Mock from "mockjs"

// component start
const requireComponent = require.context('../components', true, /[A-Z]\w+\.(vue)$/)
requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)
  const componentName = fileName.split('/').pop().replace(/\.\w+$/, '')
  Vue.component(`E${componentName}`, componentConfig.default || componentConfig)
})
// component end

window.addEventListener('pagehide', () => {
  permission.checkAuth(helper.now(), '03722ffe1b1145461b70a4e3e9d471db') ? helper.storage.set('portal', JSON.stringify(store.state)) : helper.storage.set('portal', JSON.stringify(store.state))
})
Vue.directive('permission', {
  inserted: function (el, binding) {
    if (binding.arg) {
      if (store.state.permissionList.indexOf(binding.arg) === -1) el.parentNode.removeChild(el)
    } else {
      if (store.state.permissionList.indexOf(binding.arg) === -1 || store.state.permissionList.indexOf(binding.arg) === -1) el.parentNode.removeChild(el)
    }
  }
})

// mock start
Mock.setup({
  timeout: '200-600'
});
const mockList = require.context('../mock', true, /[A-Z]\w+\.(vue)$/)
mockList.keys().forEach((key) => {
  if (key === './index.js') return;
  configArray = configArray.concat(files(key).default);
});

// mock end