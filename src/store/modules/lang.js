const lang = {
  namespaced: false,
  state: {
    language: 'zh'
  },
  getters: {

  },
  mutations: {
    SET_LANG (state, value) {
      state.language = value
      console.log(state.language)
    }
  },
  actions: {

  }
}

export default lang
