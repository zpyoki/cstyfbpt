export default [
  {
    path: '/403',
    name: '403',
    component: () => import('@/views/error/403.vue')
  }
]
