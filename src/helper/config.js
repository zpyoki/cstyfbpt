export default {
  // 启用encrypt
  encrypt: {
    enable: false,
    key: 'abcdefghigklmnopqrstuvwxyz123456',
    iv: 'abcdefghigklmnop',
    salt: 'dinkstudio'
  },
  // 启用mock
  mock: true,
  // mock: false,
  // 启用ws
  ws: true,
  // ws: false,
  // defaultUrl
  api: {
    login: '/ccs/sys/login',
    menu: '/ccs/ni/niMenu/getMenuList',
    recommend: '/ccs/ni/niMenu/getIndexMainList',
    item: '/ccs/ni/niMenu/getInfoByContentId',
    GetAll:"http://www.bingjs.com:81/Grade/GetAll",
    Delete:"http://www.bingjs.com:81/Subject/Delete",
  },
  projectManagementGuide: 'http://www.baidu.com'
}
