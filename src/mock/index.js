
//此文档用来见虚拟数据
import Mock from "mockjs"


// 设置拦截ajax请求的相应时间(请求延时时间)
Mock.setup({
  timeout: '200-600'
});

let configArray = [];

// 使用webpack的require.context()遍历所有mock文件
const files = require.context('.', true, /\.js$/);
files.keys().forEach((key) => {
  if (key === './index.js') return;
  configArray = configArray.concat(files(key).default);
});

// 注册所有的mock服务
configArray.forEach((item) => {
  for (let [path, target] of Object.entries(item)) {
    let protocol = path.split('|');
    Mock.mock(new RegExp('^' + protocol[1]), protocol[0], target);
  }
});


// 生成subjects、grades数据
const { subjects, grades } = Mock.mock({
  "grades|3": [{
      // 属性 GradeId 是一个自增数，起始值为 1，每次增 1
      'GradeId|+1': 1,
      // @cname 随机生成一个常见的中文姓名。
      "GradeName": '@cname'
  }],
  // 随机生成一个5到10条的数组
  // 属性 subjects 的值是一个数组，其中含有 5 到 10 个元素
  "subjects|5-10": [{
      'SubjectId|+1': 1,
      // @ctitle 随机生成一句中文标题。
      SubjectName: '@ctitle(10,15)',
      // @integer( min, max )返回一个随机的整数。min最小值,max最大值
      ClassHour: '@integer(22,80)',
      GradeId: '@integer(1,3)',
  }]
});
// 给课程数组添加年级信息
subjects.forEach(r => {
  // 给每个课程信息，添加一个年级的完整信息
  r.Grade = {
      GradeId: r.GradeId,
      GradeName: grades.find(g => g.GradeId == r.GradeId).GradeName
  }
});
// 拦截查询所有年级信息的请求
Mock.mock('http://www.bingjs.com:81/Grade/GetAll', "get", () => {
    return grades;
});
// 拦截删除请求
// Mock.mock('http://www.bingjs.com:81/Subject/Delete', "post", (options) => {
//     // 获取课程编号
//     let subjectId = JSON.parse(options.body)
//     // 获取课程在数组中的位置
//     let index = subjects.findIndex(r => r.SubjectId == subjectId)
//     // 删除
//     subjects.splice(index, 1)
//     return true
// });