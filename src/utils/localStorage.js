// import { analyzethetokenvalueinformation } from '@/api/login/index'
// import store from '@/store/index'
import { removeToken } from '@/utils/cookies'

export const setItem = (key, value) => {
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  window.localStorage.setItem(key, value)
}
export const getItem = key => {
  const value = window.localStorage.getItem(key)
  try {
    return JSON.parse(value)
  } catch (err) {
    return value
  }
}
// SHANCHUBENDISHUJU
export const deleteItem = key => {
  window.localStorage.removeItem(key)
}
// HUOQUBENCISHIJIAN
export const getTime = () => {
  const date = new Date().getTime() + (5000)
  setItem('time', date)
  return date
}
// JISUANDANGQIANTOKENSHIFOUGUOQI
export const tokenExpressInTime = () => {
  const data = new Date().getTime()
  const flagBool = (data - Number(getItem('time'))) > 0
  if (flagBool) {
    // 当登录时间小于现在时间6个小时说明token过期
    removeToken()
    return false
  } else {
    return false
  }
  // return (data - Number(getItem('time'))) > 0 ? true : false
}
