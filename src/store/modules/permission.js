import { constantRoutes, asyncRoutes } from '@/router'
import { menuData } from '@/router/menus'
import _ from 'lodash'
// -------筛选路由
const hasPermission = (roles, route) => {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route?.meta?.roles.includes(role))
  } else {
    return true
  }
}
export const filterAsyncRoutes = (routes, roles) => {
  const res = []
  routes.forEach(route => {
    const r = { ...route }
    if (hasPermission(roles, r)) {
      if (r.children) {
        r.children = filterAsyncRoutes(r.children, roles)
      }
      res.push(r)
    }
  })
  return res
}
// -------- 筛选菜单
export const filterAsyncMenus = (menuList, routeNames) => {
  const treeFilter = (treeNodes) => {
    for (let index = 0; index < treeNodes.length; index++) {
      const treeNode = treeNodes[index]
      const codeName = treeNode.name
      if (!routeNames.includes(codeName)) {
        treeNodes.splice(index, 1)
        index -= 1
      } else {
        if (treeNode.children && treeNode.children.length > 0) {
          treeFilter(treeNode.children)
        }
      }
    }
  }
  treeFilter(menuList)
  return menuList
}
// 递归取出所有路由name
const getRouteName = (routes) => {
  const nameList = []
  const setList = (arr) => {
    arr.forEach(item => {
      nameList.push(item.name)
      if (item.children && item.children.length > 0) {
        setList(item.children)
      }
    })
  }
  setList(routes)
  return nameList
}

const Permission = {
  namespaced: false,
  state: {
    routes: [],
    dynamicRoutes: [],
    menuList: [],
    permissionList: []
  },
  getters: {
    menuList: (state) => state.menuList,
    permissionList: state => state.permissionList,
    routes: state => state.routes,
    dynamicRoutes: state => state.dynamicRoutes
  },
  mutations: {
    SET_MENU (state, value) {
      state.menuList = value
    },
    SET_PERMISSION (state, value) {
      state.permissionList = value
    },
    SET_ROUTES (state, routes) {
      state.routes = constantRoutes.concat(routes)
      state.dynamicRoutes = routes
    }
  },
  actions: {
    async initPermission ({ commit, state }, params) {
      // 请求获取菜单接口
      //   const { data } = await getMenuApi(params)
      // const data = {
      //   menuList: [
      //     {
      //       name: 'dashboard',
      //       title: '工作台',
      //       icon: 'appstore'
      //     },

      //     // {
      //     //   name: 'registerlogin',
      //     //   title: '登录界面',
      //     //   icon: 'appstore',
      //     //   children: [],
      //     // },
      //     {
      //       name: 'demoMenu',
      //       title: '组件demo',
      //       icon: 'appstore',
      //       children: [
      //         { name: 'table', title: 'table表格' },
      //         { name: 'from', title: 'from表单' },
      //         { name: 'ajax', title: 'ajax跳转' },
      //         {
      //           name: 'Basicpage',
      //           title: '基础详情页'
      //         },
      //         {
      //           name: 'advancedpage',
      //           title: '高级详情页'
      //         },
      //         {
      //           name: 'search',
      //           title: '搜索列表'
      //         },
      //         {
      //           name: 'Inquire',
      //           title: '查询表格'
      //         },
      //         {
      //           name: 'customadd',
      //           title: '自定义组件集合'
      //         }
      //       ]
      //     },
      //     {
      //       name: 'Permission',
      //       icon: 'appstore',
      //       title: '权限菜单',
      //       children: [
      //         {
      //           name: 'AuthPageA',
      //           title: '权限测试页A'
      //         },
      //         {
      //           name: 'AuthPageB',
      //           title: '权限测试页B'
      //         },
      //         {
      //           name: 'AuthPageC',
      //           title: '权限测试页C'
      //         }
      //       ]
      //     }
      //   ]
      // }
      commit('SET_PERMISSION', getRouteName(state.routes))
      commit('SET_MENU', filterAsyncMenus(_.cloneDeep(menuData), state.permissionList))
    },
    GenerateRoutes ({ commit, dispatch, state }, roles) {
      let accessedRoutes = asyncRoutes
      // TODO: 根据权限 构建路由
      if (roles.includes('admin')) {
        accessedRoutes = asyncRoutes
      } else {
        accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      }
      commit('SET_ROUTES', accessedRoutes)
      dispatch('initPermission')
    }
  }
}

export default Permission
