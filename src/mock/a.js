// 拦截删除请求
// Mock.mock('http://www.bingjs.com:81/Subject/Delete', "post", (options) => {
//     // 获取课程编号
//     let subjectId = JSON.parse(options.body)
//     // 获取课程在数组中的位置
//     let index = subjects.findIndex(r => r.SubjectId == subjectId)
//     // 删除
//     subjects.splice(index, 1)
//     return true
// });

export default {
    'POST /Subject/Delete': {
        code: 0,
        data: {
            pageNum: 1,
            list: [{
                // 属性 GradeId 是一个自增数，起始值为 1，每次增 1
                'GradeId|+1': 1,
                // @cname 随机生成一个常见的中文姓名。
                "GradeName": '@cname'
            }]
        }
    }
}