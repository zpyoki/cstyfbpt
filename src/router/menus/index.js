export const menuData = [
  {
    name: 'dashboard',
    title: '工作台',
    icon: 'appstore'
  },
  {
    name: 'demoMenu',
    title: '组件demo',
    icon: 'appstore',
    children: [
      { name: 'table', title: 'table表格' },
      { name: 'from', title: 'from表单' },
      { name: 'ajax', title: 'ajax跳转' },
      {
        name: 'Basicpage',
        title: '基础详情页'
      },
      {
        name: 'advancedpage',
        title: '高级详情页'
      },
      {
        name: 'search',
        title: '搜索列表'
      },
      {
        name: 'Inquire',
        title: '查询表格'
      },
      {
        name: 'customadd',
        title: '自定义组件集合'
      }
    ]
  },
  {
    name: 'Permission',
    icon: 'appstore',
    title: '权限菜单',
    children: [
      {
        name: 'AuthPageA',
        title: '权限测试页A'
      },
      {
        name: 'AuthPageB',
        title: '权限测试页B'
      },
      {
        name: 'AuthPageC',
        title: '权限测试页C'
      }
    ]
  }
]
