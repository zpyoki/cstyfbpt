/**
 * Global authority directive
 * Used for fine-grained control of component permissions
 * @Example v-auth="roleEnum.TEST"
 */

import { usePermission } from '../helper/permission'

function isAuth (el, binding) {
  const { hasPermission } = usePermission()
  const value = binding.value
  // eslint-disable-next-line no-useless-return
  if (!value) return
  if (!hasPermission(value)) {
    // eslint-disable-next-line no-unused-expressions
    el.parentNode?.removeChild(el)
    // el.remove()
  }
}

const inserted = (el, binding) => {
  isAuth(el, binding)
}

const authDirective = {
  inserted
}

export function setupPermissionDirective (app) {
  app.directive('auth', authDirective)
}
export default authDirective
