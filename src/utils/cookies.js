import Cookies from 'js-cookie'

const tokenKey = 'token'
export const getToken = () => Cookies.get(tokenKey)

export const setToken = (token) => Cookies.set(tokenKey, token)

export const removeToken = () => Cookies.remove(tokenKey)

export const getCookie = key => {
  return Cookies.get(key)
}
export const setCookie = (key, value) => {
  return Cookies.set(key, value)
}
export const removeCookie = key => {
  return Cookies.remove(key)
}
