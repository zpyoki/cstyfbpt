import Vue from 'vue'
import Vuex from 'vuex'
import helper from '@/helper/helper'
// import http from '@/helper/http'
import router from '@/router'
import User from './modules/user'
import Permission from './modules/permission'
import Lang from './modules/lang'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  actions: {},
  mutations: {
    // login (state, value) {
    //   state.token = value.token
    //   state.realname = value.realname
    //   setToken(value.token)
    // },
    // logout (state) {
    //   state.token = undefined
    //   removeToken()
    //   state = {}
    //   helper.storage.clear()
    //   router.push({ name: 'login' })
    // },
    // initPermission (state, value) {
    //   state.menuList = value.menuList
    //   state.permissionList = value.permissionList
    // }
  },
  getters: {},
  modules: {
    User,
    Permission,
    Lang
  }
})
