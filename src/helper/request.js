import axios from 'axios'
// import config from '@/helper/config'
// import helper from '@/helper/helper'
// import dink from 'dinkjs'
// import router from '@/router'

// 创建axios实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: '/' + process.env.VUE_APP_API_PREFIX,
  // timeout
  timeout: 60 * 1000
})

// 请求拦截器
request.interceptors.request.use((config) => {
  // baseUrl
  // config.baseURL = '/' + process.env.VUE_APP_API_PREFIX
  // timeout
  // config.timeout = 60 * 1000
  // heartbeat
  // dink.setStorage('lastRequestTime', dink.now())
  // header
  // config.headers.Encrypt = configLocal.encrypt.enable
  // if (helper.storage.get('token')) {
  //   Object.assign(config.headers, { 'X-Access-Token': helper.storage.get('token') })
  // }
  // return config
}, (error) => {
  return Promise.reject(error)
})

// 响应拦截器
request.interceptors.response.use((response) => {
  console.log(response)
  return response.data
}, (error) => {
  // if (error.response.status === 500 && error.response.data.message === 'Token失效，请重新登录') {
  //   helper.msg.error('登录超时')
  //   helper.storage.clear()
  //   router.push({ name: 'login' })
  // } else {
  //   helper.msg.error(error.message)
  // }
  return Promise.reject(error)
})

export default request
