/* vue混入 antd icon 多语言 */
export const langMixin = {
  data () {
    return {
      lang: this.$store.state.Lang.language
    }
  },
  mounted () {
    this.onMixin()
  },
  // watch: {
  //   lang: {
  //     handler (newVal, oldVal) {
  //       this.$store.state.Lang.language = newVal
  //     },
  //     immediate: true
  //   }
  // },
  methods: {
    onMixin () {
      // console.log(this.$store.state.Lang.language)
      if (this.$store.state.Lang.language === 'zh') {
        this.$i18n.locale = this.$store.state.Lang.language
      } else {
        this.$i18n.locale = this.$store.state.Lang.language
      }
    }
  }

}
