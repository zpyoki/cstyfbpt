import { setToken, removeToken } from '@/utils/cookies'
import helper from '@/helper/helper'
import store from '../index'
import router, { resetRouter } from '@/router'
const User = {
  namespaced: false,
  state: {
    userInfo: {},
    roles: []
  },
  getters: {
    userInfo (state) {
      return state.userInfo
    },
    roles (state) {
      return state.roles
    }
  },
  mutations: {
    SET_USER_INFO (state, value) {
      state.userInfo = value
    },
    SET_ROLES (state, value) {
      state.roles = value
    }
  },
  actions: {
    async login ({ commit }, params) {
    //  调用登录接口
    //   const result = await loginApi(params)
      const result = {
        userId: '1',
        username: 'Enfi',
        realName: 'Enfi Admin',
        avatar: 'https://q1.qlogo.cn/g?b=qq&nk=190848757&s=640',
        desc: 'manager',
        token: 'fakeToken1',
        roles: ['test']
      }
      commit('SET_USER_INFO', result)
      commit('SET_ROLES', result.roles || [])
      setToken(result.token)
      helper.pageHide()
      return result
    },
    logout ({ commit }) {
      commit('SET_USER_INFO', {})
      commit('SET_ROLES', [])
      store.commit('SET_MENU', [])
      store.commit('SET_PERMISSION', [])
      resetRouter()
      removeToken()
      helper.storage.clear()
      router.push({ name: 'login' })
    }
  }
}

export default User
